#!/usr/bin/env python

from fabric.api import run, sudo
from dlabcl.common import printinfo


def install_webserver():
    printinfo('Webserver installation section')
    run('apt-get install -y apache2')


def remove_webserver():
    printinfo('Remove webserver section')
    run('apt-get purge -y apache2')


def remote_uname():
    printinfo('Webserver installation section')
    run('uname -r')
