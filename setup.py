#!/usr/bin/env python

from setuptools import setup

setup(name='dlabcl',
      version='0.1',
      description='Dlab Configuration Library',
      url='http://github.com/dlabcl',
      author='Cfg Lib',
      author_email='dlabcl@example.com',
      license='MIT',
      packages=['dlabcl','dlabcl.apache'],
      zip_safe=False,
      install_requires=["fabric"]
      )
